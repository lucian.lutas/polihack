export const USER_ROLES = {
  USER: 'user',
  RECRUITER: 'recruiter',
  ADMIN: 'admin'
}
