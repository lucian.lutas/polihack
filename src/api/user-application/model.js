import mongoose, { Schema } from 'mongoose'

const userApplicationSchema = new Schema({
  userId: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  challengeId: {
    type: Schema.ObjectId,
    ref: 'Challenge',
    required: true
  },
  files: {
    type: String,
    default: null
  },
  finishedAt: {
    type: String,
    default: null
  },
  points: {
    type: Number,
    default: 0
  },
  notes: {
    type: String,
    default: null
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

userApplicationSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      userId: this.userId,
      challengeId: this.challengeId,
      files: this.files,
      finishedAt: this.finishedAt,
      points: this.points,
      notes: this.notes,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('UserApplication', userApplicationSchema)

export const schema = model.schema
export default model
