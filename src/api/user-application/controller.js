import { success, notFound } from '../../services/response/'
import { UserApplication } from '.'

export const create = ({ bodymen: { body }, user }, res, next) =>
  UserApplication.create({
    ...body,
    userId: user.id
  })
    .then((userApplication) => userApplication.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  UserApplication.find(query, select, cursor)
    .then((userApplications) => userApplications.map((userApplication) => userApplication.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  UserApplication.findById(params.id)
    .populate('userId')
    .then(notFound(res))
    .then((userApplication) => userApplication ? userApplication.view() : null)
    .then(success(res))
    .catch(next)

export const showMyApplications = ({ user }, res, next) =>
  UserApplication.find({
    userId: user.id
  })
    .then(notFound(res))
    .then((userApplication) => userApplication ? userApplication.view() : null)
    .then(success(res))
    .catch(next)

export const getApplicationsByChallengeId = ({ params }, res, next) => {
  UserApplication.find({
    challengeId: params.id
  })
    .populate('userId')
    .then((userApplications) => userApplications.map((userApplication) => userApplication.view()))
    .then(success(res))
    .catch(next)
}

export const update = ({ bodymen: { body }, params }, res, next) =>
  UserApplication.findById(params.id)
    .then(notFound(res))
    .then((userApplication) => userApplication ? Object.assign(userApplication, body).save() : null)
    .then((userApplication) => userApplication ? userApplication.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  UserApplication.findById(params.id)
    .then(notFound(res))
    .then((userApplication) => userApplication ? userApplication.remove() : null)
    .then(success(res, 204))
    .catch(next)
