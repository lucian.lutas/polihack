import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, showMyApplications, getApplicationsByChallengeId, show, update, destroy } from './controller'
import { schema } from './model'
import { USER_ROLES } from '../../constants'
export UserApplication, { schema } from './model'

const router = new Router()
const { userId, challengeId, files, finishedAt, notes, points } = schema.tree

/**
 * @api {post} /user-applications Create user application
 * @apiName CreateUserApplication
 * @apiGroup UserApplication
 * @apiPermission user
 * @apiParam {String} access_token admin access token.
 * @apiParam challengeId User application's challengeId.
 * @apiSuccess {Object} userApplication User application's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 User application not found.
 * @apiError 401 admin access only.
 */
router.post('/',
  token({ required: true }),
  body({ challengeId }),
  create)

/**
 * @api {get} /user-applications Retrieve user applications
 * @apiName RetrieveUserApplications
 * @apiGroup UserApplication
 * @apiPermission user
 * @apiParam {String} access_token admin access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} userApplications List of user applications.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 admin access only.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /user-applications/me Retrieve current user applications
 * @apiName RetrieveCurrentUserApplications
 * @apiGroup UserApplication
 * @apiPermission user
 * @apiParam {String} access_token access token.
 * @apiSuccess {Object[]} userApplications List of current user's applications data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 User application not found.
 * @apiError 401 user access only.
 */
router.get('/me',
  token({ required: true }),
  showMyApplications)

/**
 * @api {get} /user-applications/challenge/:id Retrieve user applications by challenge id
 * @apiName RetrieveUserApplicationsByChallengeId
 * @apiGroup UserApplication
 * @apiPermission user
 * @apiParam {String} access_token admin access token.
 * @apiSuccess {Object[]} userApplications List of user applications.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 admin access only.
 */
router.get('/challenge/:id',
  token({ required: true }),
  getApplicationsByChallengeId)

/**
 * @api {get} /user-applications/:id Retrieve user application
 * @apiName RetrieveUserApplication
 * @apiGroup UserApplication
 * @apiPermission user
 * @apiParam {String} access_token admin access token.
 * @apiSuccess {Object} userApplication User application's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 User application not found.
 * @apiError 401 admin access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /user-applications/:id Update user application
 * @apiName UpdateUserApplication
 * @apiGroup UserApplication
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam userId User application's userId.
 * @apiParam challengeId User application's challengeId.
 * @apiParam files User application's files.
 * @apiParam finishedAt User application's finishedAt.
 * @apiParam points User application's points.
 * @apiSuccess {Object} userApplication User application's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 User application not found.
 * @apiError 401 admin access only.
 */
router.put('/:id',
  token({ required: true, roles: [USER_ROLES.ADMIN] }),
  body({ userId, challengeId, files, finishedAt, notes, points }),
  update)

/**
 * @api {delete} /user-applications/:id Delete user application
 * @apiName DeleteUserApplication
 * @apiGroup UserApplication
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 User application not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true, roles: [USER_ROLES.ADMIN] }),
  destroy)

export default router
