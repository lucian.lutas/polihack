import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, getByCategoryId, show, getCompanyFilteredChallenges, update, destroy } from './controller'
import { schema } from './model'
import { USER_ROLES } from '../../constants'

export Company, { schema } from './model'

const router = new Router()
const { name, city, logo, description } = schema.tree

/**
 * @api {post} /companies Create company
 * @apiName CreateCompany
 * @apiGroup Company
 * @apiPermission admin, recruiter
 * @apiParam {String} access_token admin or recruiter access token.
 * @apiParam {String} name Company's name.
 * @apiParam {String} city Company's city.
 * @apiParam {String} logo Company's logo.
 * @apiParam {String} description Company's description (min 250 chars).
 * @apiSuccess {Object} company Company's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Company not found.
 * @apiError 401 admin access only.
 */
router.post('/',
  token({ required: true, roles: [USER_ROLES.ADMIN, USER_ROLES.RECRUITER] }),
  body({ name, city, logo, description }),
  create)

/**
 * @api {get} /companies Retrieve companies
 * @apiName RetrieveCompanies
 * @apiGroup Company
 * @apiUse listParams
 * @apiSuccess {Object[]} companies List of companies.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 admin access only.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /companies/category/:id Retrieve companies by category id
 * @apiName RetrieveCompaniesByCategoryId
 * @apiGroup Company
 * @apiSuccess {Object[]} companies List of companies by category id
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Company not found.
 * @apiError 401 admin access only.
 */
router.get('/category/:id',
  getByCategoryId)

/**
 * @api {get} /companies/:id Retrieve company
 * @apiName RetrieveCompany
 * @apiGroup Company
 * @apiSuccess {Object} company Company's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Company not found.
 * @apiError 401 admin access only.
 */
router.get('/:id',
  show)

/**
 * @api {get} /companies/:id/filtered-challenges Retrieve company with filtered challenges
 * @apiName RetrieveCompanyFilteredChallenges
 * @apiGroup Company
 * @apiParam {String[]} categoryIds List of categoryIds.
 * @apiSuccess {Object} company Company's data with filtered challenges.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Company not found.
 * @apiError 401 admin access only.
 */
router.get('/:id/filtered-challenges',
  getCompanyFilteredChallenges)

/**
 * @api {put} /companies/:id Update company
 * @apiName UpdateCompany
 * @apiGroup Company
 * @apiPermission admin, recruiter
 * @apiParam {String} access_token admin or recruiter access token.
 * @apiParam {String} name Company's name.
 * @apiParam {String} city Company's city.
 * @apiParam {String} logo Company's logo.
 * @apiParam {String} description Company's description (min 250 chars).
 * @apiSuccess {Object} company Company's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Company not found.
 * @apiError 401 admin access only.
 */
router.put('/:id',
  token({ required: true, roles: [USER_ROLES.ADMIN, USER_ROLES.RECRUITER] }),
  body({ name, city, logo, description }),
  update)

/**
 * @api {delete} /companies/:id Delete company
 * @apiName DeleteCompany
 * @apiGroup Company
 * @apiPermission admin, recruiter
 * @apiParam {String} access_token admin or recruiter access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Company not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true, roles: [USER_ROLES.ADMIN, USER_ROLES.RECRUITER] }),
  destroy)

export default router
