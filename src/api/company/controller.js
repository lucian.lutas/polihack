import { success, notFound } from '../../services/response/'
import { Company } from '.'
import { Challenge } from '../challenge'

export const create = ({ bodymen: { body }, user }, res, next) =>
  Company.create({
    ...body,
    administratorId: user.id
  })
    .then(async (company) => {
      await Object.assign(user, { companyId: company.id }).save()
      return company.view(true)
    })
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Company.find(query, select, cursor)
    .then((companies) => companies.map((company) => company.view()))
    .then(success(res))
    .catch(next)

export const getByCategoryId = async ({ params, user }, res, next) => {
  try {
    const challenges = await Challenge.find({ categoryId: params.id })
    const companyIds = []
    challenges.forEach((challenge) => {
      companyIds.push(challenge.companyId)
    })
    const companies = await Company.find({
      id: { $in: companyIds }
    })
    companies.map((company) => company.view())
    return companies
  } catch (e) {
    next(res)
  }
}

export const show = ({ params }, res, next) =>
  Company.findById(params.id)
    .then(notFound(res))
    .then((company) => company ? company.view() : null)
    .then(success(res))
    .catch(next)

export const getCompanyFilteredChallenges = async ({ body, params, user }, res, next) => {
  try {
    const { categoryIds } = body
    if (typeof categoryIds.isArray === 'undefined') {
      return res.status(400).json({
        valid: false,
        param: 'categoryIds',
        message: 'categoryIds must be an array'
      })
    }
    const company = await Company.findById(params.id)
    const challenges = await Challenge.find({
      categoryId: { $in: categoryIds },
      companyId: params.id
    })
    challenges.map((challenge) => challenge.view())
    return {
      company,
      challenges
    }
  } catch (e) {
    next(res)
  }
}

export const update = ({ bodymen: { body }, params }, res, next) =>
  Company.findById(params.id)
    .then(notFound(res))
    .then((company) => company ? Object.assign(company, body).save() : null)
    .then((company) => company ? company.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Company.findById(params.id)
    .then(notFound(res))
    .then((company) => company ? company.remove() : null)
    .then(success(res, 204))
    .catch(next)
