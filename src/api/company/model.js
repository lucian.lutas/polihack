import mongoose, { Schema } from 'mongoose'

const companySchema = new Schema({
  name: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  },
  logo: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true,
    minlength: 250
  },
  isPremium: {
    type: Boolean,
    default: false
  },
  administratorId: {
    type: Schema.ObjectId,
    ref: 'User'
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

companySchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      name: this.name,
      city: this.city,
      logo: this.logo,
      description: this.description,
      isPremium: this.isPremium,
      administrator: this.administrator,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Company', companySchema)

export const schema = model.schema
export default model
