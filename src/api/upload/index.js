import { Router } from 'express'
import { token } from '../../services/passport'
import { uploadFile, uploadImage } from './controller'

const router = new Router()

/**
 * @api {post} /uploads/file Upload file
 * @apiName UploadFile
 * @apiGroup Upload
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam {Form-Data} file File to be uploaded.
 * @apiSuccess {Object} upload Upload's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Upload not found.
 * @apiError 401 admin access only.
 */
router.post('/file',
  token({ required: true }),
  uploadFile)

/**
 * @api {post} /uploads/image Upload image
 * @apiName UploadImage
 * @apiGroup Upload
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam {Form-Data} file File to be uploaded.
 * @apiSuccess {Object} upload Upload's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Upload not found.
 * @apiError 401 admin access only.
 */
router.post('/image',
  token({ required: true }),
  uploadImage)

export default router
