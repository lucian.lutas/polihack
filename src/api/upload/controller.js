import { uploadFileFromBuffer } from '../../services/upload'

export const uploadFile = async ({ file, user }, res, next) => {
  try {
    const { originalname, buffer, size } = file
    const fileExtension = originalname.substr(file.originalname.lastIndexOf('.') + 1).toLowerCase()
    if (fileExtension !== 'pdf' && fileExtension !== 'zip') {
      return res.status(400).json({
        valid: false,
        message: 'only PDF and ZIP allowed'
      })
    }
    if (size > 30000000) {
      return res.status(400).json({
        valid: false,
        message: 'file must be under 30MB'
      })
    }
    const uploadPath = `user_${user.id}/files`
    const fileName = Date.now()
    const uploadResponse = await uploadFileFromBuffer(buffer, uploadPath, fileName, fileExtension)
    res.status(201).json({
      valid: true,
      url: uploadResponse.Location
    })
  } catch (e) {
    next(e)
  }
}

export const uploadImage = async ({ file, user }, res, next) => {
  try {
    const { originalname, buffer, size } = file
    const fileExtension = originalname.substr(file.originalname.lastIndexOf('.') + 1).toLowerCase()
    if (fileExtension !== 'png' && fileExtension !== 'jpg' && fileExtension !== 'jpeg') {
      return res.status(400).json({
        valid: false,
        message: 'only static images allowed'
      })
    }
    if (size > 5000000) {
      return res.status(400).json({
        valid: false,
        message: 'image must be under 5MB'
      })
    }
    const uploadPath = `user_${user.id}/images`
    const fileName = Date.now()
    const uploadResponse = await uploadFileFromBuffer(buffer, uploadPath, fileName, fileExtension)
    res.status(201).json({
      valid: true,
      url: uploadResponse.Location
    })
  } catch (e) {
    next(e)
  }
}
