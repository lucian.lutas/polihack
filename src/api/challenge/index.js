import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, getChallengesByCompanyId, show, update, destroy } from './controller'
import { schema } from './model'
import { USER_ROLES } from '../../constants'

export Challenge, { schema } from './model'

const router = new Router()
const { name, description, position, files, categoryId, startAt, endAt } = schema.tree

/**
 * @api {post} /challenges Create challenge
 * @apiName CreateChallenge
 * @apiGroup Challenge
 * @apiPermission admin, recruiter
 * @apiParam {String} access_token admin or recruiter access token.
 * @apiParam name Challenge's name.
 * @apiParam description Challenge's description.
 * @apiParam position Challenge's position.
 * @apiParam files Challenge's files.
 * @apiParam categoryId Challenge's categoryId.
 * @apiParam startAt Challenge's startAt.
 * @apiParam endAt Challenge's endAt.
 * @apiSuccess {Object} challenge Challenge's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Challenge not found.
 * @apiError 401 admin access only.
 */
router.post('/',
  token({ required: true, roles: [USER_ROLES.ADMIN, USER_ROLES.RECRUITER] }),
  body({ name, description, position, files, categoryId, startAt, endAt }),
  create)

/**
 * @api {get} /challenges Retrieve challenges
 * @apiName RetrieveChallenges
 * @apiGroup Challenge
 * @apiUse listParams
 * @apiSuccess {Object[]} challenges List of challenges.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 admin access only.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /challenges/company/:id Retrieve challenges by companyId
 * @apiName RetrieveChallengesByCompanyId
 * @apiGroup Challenge
 * @apiSuccess {Object[]} challenges List of challenges.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Challenge not found.
 * @apiError 401 admin access only.
 */
router.get('/company/:id',
  getChallengesByCompanyId)

/**
 * @api {get} /challenges/:id Retrieve challenge
 * @apiName RetrieveChallenge
 * @apiGroup Challenge
 * @apiSuccess {Object} challenge Challenge's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Challenge not found.
 * @apiError 401 admin access only.
 */
router.get('/:id',
  show)

/**
 * @api {put} /challenges/:id Update challenge
 * @apiName UpdateChallenge
 * @apiGroup Challenge
 * @apiPermission admin, recruiter
 * @apiParam {String} access_token admin or recruiter access token.
 * @apiParam name Challenge's name.
 * @apiParam description Challenge's description.
 * @apiParam position Challenge's position.
 * @apiParam files Challenge's files.
 * @apiParam companyId Challenge's companyId.
 * @apiParam categoryId Challenge's categoryId.
 * @apiParam startAt Challenge's startAt.
 * @apiParam endAt Challenge's endAt.
 * @apiSuccess {Object} challenge Challenge's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Challenge not found.
 * @apiError 401 admin access only.
 */
router.put('/:id',
  token({ required: true, roles: [USER_ROLES.ADMIN, USER_ROLES.RECRUITER] }),
  body({ name, description, position, files, categoryId, startAt, endAt }),
  update)

/**
 * @api {delete} /challenges/:id Delete challenge
 * @apiName DeleteChallenge
 * @apiGroup Challenge
 * @apiPermission admin, recruiter
 * @apiParam {String} access_token admin or recruiter access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Challenge not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true, roles: [USER_ROLES.ADMIN, USER_ROLES.RECRUITER] }),
  destroy)

export default router
