import mongoose, { Schema } from 'mongoose'

const challengeSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true,
    minlength: 100
  },
  position: {
    type: String,
    required: true
  },
  files: {
    type: String,
    required: true
  },
  companyId: {
    type: Schema.ObjectId,
    ref: 'Company',
    required: true
  },
  categoryId: {
    type: Schema.ObjectId,
    ref: 'Category',
    required: true
  },
  startAt: {
    type: Date,
    required: true
  },
  endAt: {
    type: Date,
    required: true
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

challengeSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      name: this.name,
      description: this.description,
      position: this.position,
      files: this.files,
      companyId: this.companyId,
      categoryId: this.categoryId,
      startAt: this.startAt,
      endAt: this.endAt,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Challenge', challengeSchema)

export const schema = model.schema
export default model
