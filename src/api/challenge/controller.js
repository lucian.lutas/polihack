import { success, notFound } from '../../services/response/'
import { Challenge } from '.'

export const create = ({ bodymen: { body }, user }, res, next) =>
  Challenge.create({
    ...body,
    companyId: user.companyId
  })
    .then((challenge) => challenge.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Challenge.find(query, select, cursor)
    .then((challenges) => challenges.map((challenge) => challenge.view()))
    .then(success(res))
    .catch(next)

export const getChallengesByCompanyId = ({ params }, res, next) => {
  Challenge.find({
    companyId: params.id
  })
    .then((challenges) => challenges.map((challenge) => challenge.view()))
    .then(success(res))
    .catch(next)
}

export const show = ({ params }, res, next) =>
  Challenge.findById(params.id)
    .then(notFound(res))
    .then((challenge) => challenge ? challenge.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Challenge.findById(params.id)
    .then(notFound(res))
    .then((challenge) => challenge ? Object.assign(challenge, body).save() : null)
    .then((challenge) => challenge ? challenge.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Challenge.findById(params.id)
    .then(notFound(res))
    .then((challenge) => challenge ? challenge.remove() : null)
    .then(success(res, 204))
    .catch(next)
