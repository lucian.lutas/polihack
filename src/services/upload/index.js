import { s3 as s3Config } from '../../config'
import aws from 'aws-sdk'

aws.config.update({
  secretAccessKey: s3Config.secretAccessKey,
  accessKeyId: s3Config.accessKeyId,
  region: s3Config.region
})
const s3 = new aws.S3()

export const uploadFileFromBuffer = (fileBuffer, uploadPath, fileName, extension) => {
  return new Promise((resolve, reject) => {
    s3.upload(
      {
        ACL: 'public-read',
        Bucket: s3Config.bucketName,
        Key: `${uploadPath}/${fileName}.${extension}`,
        Body: fileBuffer
      },
      (err, result) => {
        if (err) {
          reject(err)
          return
        }
        resolve(result)
      })
  })
}
