# polihack v1.0.0



- [Auth](#auth)
	- [Authenticate](#authenticate)
	- [Authenticate with Facebook](#authenticate-with-facebook)
	- [Authenticate with Google](#authenticate-with-google)
	
- [Category](#category)
	- [Create category](#create-category)
	- [Delete category](#delete-category)
	- [Retrieve categories](#retrieve-categories)
	- [Retrieve category](#retrieve-category)
	- [Update category](#update-category)
	
- [Challenge](#challenge)
	- [Create challenge](#create-challenge)
	- [Delete challenge](#delete-challenge)
	- [Retrieve challenge](#retrieve-challenge)
	- [Retrieve challenges](#retrieve-challenges)
	- [Retrieve challenges by companyId](#retrieve-challenges-by-companyid)
	- [Update challenge](#update-challenge)
	
- [Company](#company)
	- [Create company](#create-company)
	- [Delete company](#delete-company)
	- [Retrieve companies](#retrieve-companies)
	- [Retrieve companies by category id](#retrieve-companies-by-category-id)
	- [Retrieve company](#retrieve-company)
	- [Retrieve company with filtered challenges](#retrieve-company-with-filtered-challenges)
	- [Update company](#update-company)
	
- [PasswordReset](#passwordreset)
	- [Send email](#send-email)
	- [Submit password](#submit-password)
	- [Verify token](#verify-token)
	
- [Upload](#upload)
	- [Upload file](#upload-file)
	- [Upload image](#upload-image)
	
- [UserApplication](#userapplication)
	- [Create user application](#create-user-application)
	- [Delete user application](#delete-user-application)
	- [Retrieve current user applications](#retrieve-current-user-applications)
	- [Retrieve user application](#retrieve-user-application)
	- [Retrieve user applications](#retrieve-user-applications)
	- [Retrieve user applications by challenge id](#retrieve-user-applications-by-challenge-id)
	- [Update user application](#update-user-application)
	
- [User](#user)
	- [Create user](#create-user)
	- [Delete user](#delete-user)
	- [Retrieve current user](#retrieve-current-user)
	- [Retrieve user](#retrieve-user)
	- [Retrieve users](#retrieve-users)
	- [Update password](#update-password)
	- [Update user](#update-user)
	


# Auth

## Authenticate



	POST /auth

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|

## Authenticate with Facebook



	POST /auth/facebook


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Facebook user accessToken.</p>							|

## Authenticate with Google



	POST /auth/google


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Google user accessToken.</p>							|

# Category

## Create category



	POST /categories


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| name			| 			|  <p>Category's name.</p>							|

## Delete category



	DELETE /categories/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Retrieve categories



	GET /categories


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Retrieve category



	GET /categories/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Update category



	PUT /categories/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| name			| 			|  <p>Category's name.</p>							|

# Challenge

## Create challenge



	POST /challenges


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or recruiter access token.</p>							|
| name			| 			|  <p>Challenge's name.</p>							|
| description			| 			|  <p>Challenge's description.</p>							|
| position			| 			|  <p>Challenge's position.</p>							|
| files			| 			|  <p>Challenge's files.</p>							|
| categoryId			| 			|  <p>Challenge's categoryId.</p>							|
| startAt			| 			|  <p>Challenge's startAt.</p>							|
| endAt			| 			|  <p>Challenge's endAt.</p>							|

## Delete challenge



	DELETE /challenges/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or recruiter access token.</p>							|

## Retrieve challenge



	GET /challenges/:id


## Retrieve challenges



	GET /challenges


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Retrieve challenges by companyId



	GET /challenges/company/:id


## Update challenge



	PUT /challenges/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or recruiter access token.</p>							|
| name			| 			|  <p>Challenge's name.</p>							|
| description			| 			|  <p>Challenge's description.</p>							|
| position			| 			|  <p>Challenge's position.</p>							|
| files			| 			|  <p>Challenge's files.</p>							|
| companyId			| 			|  <p>Challenge's companyId.</p>							|
| categoryId			| 			|  <p>Challenge's categoryId.</p>							|
| startAt			| 			|  <p>Challenge's startAt.</p>							|
| endAt			| 			|  <p>Challenge's endAt.</p>							|

# Company

## Create company



	POST /companies


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or recruiter access token.</p>							|
| name			| String			|  <p>Company's name.</p>							|
| city			| String			|  <p>Company's city.</p>							|
| logo			| String			|  <p>Company's logo.</p>							|
| description			| String			|  <p>Company's description (min 250 chars).</p>							|

## Delete company



	DELETE /companies/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or recruiter access token.</p>							|

## Retrieve companies



	GET /companies


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Retrieve companies by category id



	GET /companies/category/:id


## Retrieve company



	GET /companies/:id


## Retrieve company with filtered challenges



	GET /companies/:id/filtered-challenges


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| categoryIds			| String[]			|  <p>List of categoryIds.</p>							|

## Update company



	PUT /companies/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin or recruiter access token.</p>							|
| name			| String			|  <p>Company's name.</p>							|
| city			| String			|  <p>Company's city.</p>							|
| logo			| String			|  <p>Company's logo.</p>							|
| description			| String			|  <p>Company's description (min 250 chars).</p>							|

# PasswordReset

## Send email



	POST /password-resets


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| email			| String			|  <p>Email address to receive the password reset token.</p>							|
| link			| String			|  <p>Link to redirect user.</p>							|

## Submit password



	PUT /password-resets/:token


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| password			| String			|  <p>User's new password.</p>							|

## Verify token



	GET /password-resets/:token


# Upload

## Upload file



	POST /uploads/file


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| file			| Form-Data			|  <p>File to be uploaded.</p>							|

## Upload image



	POST /uploads/image


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| file			| Form-Data			|  <p>File to be uploaded.</p>							|

# UserApplication

## Create user application



	POST /user-applications


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| challengeId			| 			|  <p>User application's challengeId.</p>							|

## Delete user application



	DELETE /user-applications/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Retrieve current user applications



	GET /user-applications/me


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>access token.</p>							|

## Retrieve user application



	GET /user-applications/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Retrieve user applications



	GET /user-applications


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Retrieve user applications by challenge id



	GET /user-applications/challenge/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Update user application



	PUT /user-applications/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| userId			| 			|  <p>User application's userId.</p>							|
| challengeId			| 			|  <p>User application's challengeId.</p>							|
| files			| 			|  <p>User application's files.</p>							|
| finishedAt			| 			|  <p>User application's finishedAt.</p>							|
| points			| 			|  <p>User application's points.</p>							|

# User

## Create user



	POST /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|
| email			| String			|  <p>User's email.</p>							|
| password			| String			|  <p>User's password.</p>							|
| firstName			| String			|  <p>User's first name.</p>							|
| lastName			| String			|  <p>User's last name.</p>							|

## Delete user



	DELETE /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve current user



	GET /users/me


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve user



	GET /users/:id


## Retrieve users



	GET /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update password



	PUT /users/:id/password

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| password			| String			|  <p>User's new password.</p>							|

## Update user



	PUT /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| firstName			| String			|  <p>User's first name.</p>							|
| lastName			| String			|  <p>User's last name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|


